/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "JuceHeader.h"

void print (const String& message)
{
    Logger::writeToLog (message);
}

StringArray getArgs (int argc, char* argv[])
{
    StringArray commandLine (argv + 1, argc - 1);
    StringArray args;
    args.addTokens (commandLine.joinIntoString (" "), true);
    return args;
}

bool matchArgument (StringArray args, StringArray options, StringArray& optionArgs)
{
    optionArgs.clear();
    
    bool matchedArgument = false;
    bool accumalateArgs = false;
    
    for (String* argPtr = args.begin(); argPtr != args.end(); ++argPtr)
    {
        const String& arg (*argPtr);
        
        if (accumalateArgs)
        {
            if (arg.startsWithChar ('-'))
                accumalateArgs = false;
            else
                optionArgs.add (arg);
        }
        
            
        if ( ! accumalateArgs)
        {
            for (String* optionPtr = options.begin(); optionPtr != options.end(); ++optionPtr)
            {
                if (*optionPtr == arg)
                {
                    matchedArgument = true;
                    accumalateArgs = true;
                    break;
                }
            }
        }
    }
    
    return matchedArgument;
}

void foo (StringArray options)
{
    print ("--foo");
    print (options.joinIntoString ("\n"));
}

void bar (StringArray options)
{
    print ("--bar");
    print (options.joinIntoString ("\n"));
}

//==============================================================================
int main (int argc, char* argv[])
{
    print (String ("juceval v") + ProjectInfo::versionString);
    
    StringArray args (getArgs (argc, argv));
    
    StringArray fooOptions;
    fooOptions.add ("--foo");
    fooOptions.add ("-f");
    
    StringArray barOptions;
    barOptions.add ("--bar");
    barOptions.add ("-b");
    
    StringArray optionArgs;
    if (matchArgument (args, fooOptions, optionArgs)) foo (optionArgs);
    if (matchArgument (args, barOptions, optionArgs)) bar (optionArgs);
    
    return 0;
}
